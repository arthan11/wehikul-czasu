


class PolePlanszy(object):
    def __init__(self):
        self.pola = []

class PoleWehikul(PolePlanszy):
    pass

class PoleKostka(PolePlanszy):
    def __init__(self, liczby):
        super(self.__class__, self).__init__()
        self.liczby = liczby

class PoleDinozaur(PolePlanszy):
    def __init__(self, liczby):
        super(self.__class__, self).__init__()
        self.liczby = liczby

class PoleAkcja(PolePlanszy):
    pass

class PoleRoszada(PolePlanszy):
    pass

class PoleStrzalka(PolePlanszy):
    def __init__(self):
        super(self.__class__, self).__init__()
        self.cel = None

class PoleDino(PolePlanszy):
    def __init__(self, nazwa):
        super(self.__class__, self).__init__()
        self.nazwa = nazwa

class PoleLapki(PolePlanszy):
    pass

class PoleKamien(PolePlanszy):
    pass


class TablicaPol(object):
    def __init__(self):
        self.tab = []

    def add(self, pole):
        self.tab.append(pole)
        return pole

    def lacz_kolo(self):
        last = len(self.tab) - 1
        for i, pole1 in enumerate(self.tab):
            if i < last:
                self.tab[i].pola.append(self.tab[i+1])
            else:
                self.tab[i].pola.append(self.tab[0])

            if i > 0:
                self.tab[i].pola.append(self.tab[i-1])
            else:
                self.tab[i].pola.append(self.tab[last])

class Plansza(object):
    def __init__(self):
        self.pole_startowe = PoleWehikul()

        self.zewn = TablicaPol()
        self.zewn.add(PoleKostka([1, 3, 5]))
        self.zewn.add(PoleDino('celofyz'))
        self.zewn.add(PoleAkcja())
        self.zewn.add(PoleRoszada())
        self.zewn.add(PoleDino('ankylozaur'))
        self.zewn.add(PoleLapki())
        lacz_zewn_1 = self.zewn.add(PoleStrzalka())
        self.zewn.add(PoleDino('eoraptor'))
        self.zewn.add(PoleDino('protoceratops'))
        self.zewn.add(PoleKostka([2, 4, 6]))
        self.zewn.add(PoleLapki())
        self.zewn.add(PoleKamien())
        self.zewn.add(PoleDino('tyranozaur'))
        self.zewn.add(PoleAkcja())
        lacz_zewn_2 = self.zewn.add(PoleRoszada())
        self.zewn.add(PoleDino('korytozaur'))
        self.zewn.add(PoleLapki())
        self.zewn.add(PoleKamien())
        self.zewn.add(PoleDino('iguanodon'))
        self.zewn.add(PoleDinozaur([4, 5, 6]))
        self.zewn.add(PoleAkcja())
        self.zewn.add(PoleDino('hadrozaur'))
        self.zewn.add(PoleKostka([1, 3, 5]))
        self.zewn.add(PoleDino('owiraptor'))
        self.zewn.add(PoleKamien())
        self.zewn.add(PoleRoszada())
        lacz_zewn_3 = self.zewn.add(PoleStrzalka())
        self.zewn.add(PoleLapki())
        self.zewn.add(PoleDino('euoplocefal'))
        self.zewn.add(PoleKamien())
        self.zewn.add(PoleLapki())
        self.zewn.add(PoleAkcja())
        self.zewn.add(PoleDino('majazaura'))
        self.zewn.add(PoleKostka([2, 4, 6]))
        lacz_zewn_4 = self.zewn.add(PoleStrzalka())
        self.zewn.add(PoleDino('hipsylofodon'))
        self.zewn.add(PoleLapki())
        self.zewn.add(PoleKamien())
        self.zewn.add(PoleDino('diplodok'))
        self.zewn.add(PoleDinozaur([1, 2]))
        self.zewn.lacz_kolo()

        self.wewn = TablicaPol()
        self.wewn.add(PoleLapki())
        self.wewn.add(PoleDino('parazaurolof'))
        self.wewn.add(PoleAkcja())
        self.wewn.add(PoleLapki())
        lacz_wewn_1 = self.wewn.add(PoleStrzalka())
        self.wewn.add(PoleKamien())
        self.wewn.add(PoleDino('stegozaur'))
        self.wewn.add(PoleKostka([1, 3, 5]))
        self.wewn.add(PoleAkcja())
        self.wewn.add(PoleLapki())
        lacz_wewn_2 = self.wewn.add(PoleStrzalka())
        self.wewn.add(PoleDino('brachiozaur'))
        self.wewn.add(PoleKamien())
        self.wewn.add(PoleAkcja())
        self.wewn.add(PoleDino('welociraptor'))
        self.wewn.add(PoleLapki())
        self.wewn.add(PoleRoszada())
        self.wewn.add(PoleKamien())
        lacz_wewn_3 = self.wewn.add(PoleAkcja())
        self.wewn.add(PoleDinozaur([5, 6]))
        self.wewn.add(PoleDino('giganotozaur'))
        self.wewn.add(PoleLapki())
        self.wewn.add(PoleAkcja())
        self.wewn.add(PoleDino('stegoceras'))
        lacz_wewn_4 = self.wewn.add(PoleAkcja())
        self.wewn.add(PoleKostka([2, 4, 6]))
        self.wewn.add(PoleDino('dilofozaur'))
        self.wewn.add(PoleKamien())
        self.wewn.lacz_kolo()

        self.inne = TablicaPol()

        x = self.inne.add(PoleDinozaur([3, 4]))
        self.lacznik(x, lacz_zewn_1)
        self.lacznik(x, lacz_wewn_1)

        x = self.inne.add(PoleAkcja())
        self.lacznik(x, lacz_zewn_2)
        self.lacznik(x, lacz_wewn_2)

        x = self.inne.add(PoleLapki())
        self.lacznik(x, lacz_zewn_3)
        self.lacznik(x, lacz_wewn_3)

        x = self.inne.add(PoleDino('archeopteryks'))
        self.lacznik(x, lacz_zewn_4)
        self.lacznik(x, lacz_wewn_4)

        # cele strzalek
        self.zewn.tab[6].cel = self.inne.tab[0]
        self.zewn.tab[26].cel = self.inne.tab[2]
        self.zewn.tab[34].cel = self.zewn.tab[35]
        self.wewn.tab[4].cel = self.wewn.tab[5]
        self.wewn.tab[10].cel = self.wewn.tab[9]


    def lacznik(self, pole1, pole2):
        pole1.pola.append(pole2)
        pole2.pola.append(pole1)

if __name__ == '__main__':
    plansza = Plansza()
    '''
    for pole in plansza.zewn.tab:
        if pole.__class__ == PoleDino:
            print '{}: {}'.format(pole.__class__.__name__, pole.nazwa)
        elif pole.__class__ in (PoleKostka, PoleDinozaur):
            print '{}: {}'.format(pole.__class__.__name__, pole.liczby)
        else:
            print pole.__class__.__name__

        for powiazane in pole.pola:
            print '\t- {}'.format(powiazane.__class__.__name__)
    '''

    typy = {}
    for pole in plansza.inne.tab:
        if not pole.__class__.__name__ in typy:
            typy[pole.__class__.__name__] = 1
        else:
            typy[pole.__class__.__name__] += 1


    for typ in typy:
        print '{}\t{}'.format(typy[typ], typ)
    print len(plansza.zewn.tab) + len(plansza.wewn.tab) + len(plansza.inne.tab)


